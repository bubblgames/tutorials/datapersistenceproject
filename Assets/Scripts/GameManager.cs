using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public int highScore;
    public string playerName;
    public string highScoreName;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }

        var data = SaveManager.Load();
        highScore = data.score;
        highScoreName = data.playerName;
    }

    public void SaveHighScore(int score)
    {
        var data = new SaveData
        {
            score = score,
            playerName = playerName
        };
        SaveManager.Save(data);
        highScore = score;
        highScoreName = playerName;
    }
}
