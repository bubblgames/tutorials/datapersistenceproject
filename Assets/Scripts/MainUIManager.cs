using System.Collections;
using System.Collections.Generic;
using TMPro;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainUIManager : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TMP_InputField nameInputField;
    
    void Start()
    {
        scoreText.text = "High Score: " + GameManager.instance.highScore + " : " + GameManager.instance.highScoreName;
        nameInputField.onEndEdit.AddListener(SetName);
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void Exit()
    {
#if UNITY_EDITOR
        EditorApplication.ExitPlaymode();
#else
        Application.Quit();
#endif
    }
    
    public void SetName(string inputName)
    {
        GameManager.instance.playerName = inputName;
    }
}
