using System.IO;
using UnityEngine;

public static class SaveManager
{
    public static void Save(SaveData data)
    {
        string path = Application.persistentDataPath + "/savefile.json";
        string json = JsonUtility.ToJson(data);
        File.WriteAllText(path, json);
    }

    public static SaveData Load()
    {
        string path = Application.persistentDataPath + "/savefile.json";
        if (File.Exists(path))
        {
            string json = File.ReadAllText(path);
            var data = JsonUtility.FromJson<SaveData>(json);
            return data;
        }
        else
        {
            return new SaveData
            {
                score = 0
            };
        }

    }

}